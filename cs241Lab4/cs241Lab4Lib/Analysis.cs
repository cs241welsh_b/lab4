﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs241Lab4Lib
{
    public class Analysis
    {
        static public int ThreeSums(int[] numbers)
        {
            int n = numbers.Length;
            int count = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    for (int k = j + 1; k < n; k++)
                    {
                        if (numbers[i] + numbers[j] + numbers[k] == 0) ++count;
                    }
                }
            }
            return count;
        }


        static public List<Tuple<int, int, int>> ThreeSumsTuples(int[] numbers)
        {
            //returns a list of tuples, 3 ints each, in which the 3 ints add up to 0
            List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();
            Tuple<int, int, int> threeSumSet;

            int n = numbers.Length;
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    for (int k = j + 1; k < n; k++)
                    {
                        if (numbers[i] + numbers[j] + numbers[k] == 0)
                        {
                            threeSumSet = Tuple.Create(numbers[i], numbers[j], numbers[k] );
                            result.Add(threeSumSet);
                        }
                    }
                }
            }
            return result;
        }

        static public void Sort(int[] numbers)
        {
            int temp = 0;
            bool swapsNeeded = true;

            for(int i = 0; i < numbers.Length - 1 && swapsNeeded; i++)
            {
                swapsNeeded = false;
                for (int j = 0; j < numbers.Length - 1; j++)
                {
                    if (numbers[j] > numbers[j + 1])
                    {
                        temp = numbers[j + 1];
                        numbers[j + 1] = numbers[j];
                        numbers[j] = temp;
                        swapsNeeded = true;
                    }
                }
            }
        }

        static public void MakeRandom(int[] array, int magnitude, int seed, bool includenegativevalues)
        {
            // Start and seed the RNG
            Random random = new Random(seed);

            // If includenegativevalues is true
            if (includenegativevalues)
            {
                // For each space in the array, create a random number within range -magnitude to magnitude
                for(int i = 0; i < array.Length - 1; i++)
                {
                    array[i] = random.Next(-magnitude, magnitude);
                }
            }
            else
            {
                // Otherwise, fill the array with random numbers within range 0 to magnitude
                for(int i = 0; i < array.Length; i++)
                {
                    array[i] = random.Next(0, magnitude);
                }
            }

        }

        static public int LinearSearch(int[] array, int item)
        {
            int returnValue = -1;
            for(int i = 0; i < array.Length; i++)
            {
                if (array[i] == item)
                    returnValue = i;
            }
            return returnValue;
        }

        static public int BinarySearch(int[] array, int item)
        {
            int lo = 0, hi = array.Length - 1;

            while (lo <= hi)
            {
                // Find the middle of the part of the array we are searching
                int mid = lo + (hi - lo) / 2;
                // Var that determines whether item is greater than, less than, or equal to the item at array[mid]
                int cmp = item.CompareTo(array[mid]);

                // If item is less than the item at array[mid], adjust hi bound to mid - 1, allowing us to search the left half of the array
                if (cmp < 0) hi = mid - 1;
                // Else if item is greater than the item at array[mid], adjust the lo bound to mid + 1
                else if (cmp > 0) lo = mid + 1;
                // Else, we must have found the item, so return mid
                else return mid;
            }
            // If we've reached this line without returning, item isn't in list
            return -1;
        }
    }
}

