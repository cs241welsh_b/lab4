﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cs241Lab4Lib;

namespace cs241Lab4Test
{
    [TestClass]
    public class UnitTest1
    {
        // Implement the ThreeSums algorithm from Sedgewick p173
        // Java code from Sedgewick follow: 
        //public static in count(int[] a)
        //{
        //    int n = a.length;
        //    int count = 0;
        //    for (int i = 0; i < n; i++)
        //    {
        //        for (int j = i + 1; j < n; j++ >)
        //        {
        //            for (int k = j + 1; k < n; k++ >)
        //            {
        //                if (a[i] + a[j] + a[k] == 0) ++count;
        //            }
        //        }
        //    }
        //    return count;
        //}

        [TestMethod]
        public void threeSumsCountResult()
        {
            int[] smallArray = new int[] { 1, 2, 3, -3, -2, -1, 0 };
            int result = Analysis.ThreeSums(smallArray);
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void threeSumTupleResult()
        {
            int[] smallArray = new int[] { 1, 2, 3, -3, -2, -1, 0 };
            List<Tuple<int, int, int>> result =
                Analysis.ThreeSumsTuples(smallArray);
            Assert.AreEqual(5, result.Count);
            Assert.IsTrue(result.Contains(new Tuple<int, int, int>(1, 2, -3)));
            Assert.IsTrue(result.Contains(new Tuple<int, int, int>(1, -1, 0)));
            Assert.IsTrue(result.Contains(new Tuple<int, int, int>(2, -2, 0)));
            Assert.IsTrue(result.Contains(new Tuple<int, int, int>(3, -3, 0)));
            Assert.IsTrue(result.Contains(new Tuple<int, int, int>(3, -2, -1)));
        }

        [TestMethod]
        public void sortTest()
        {
            int[] smallArray = new int[] { 1, 2, 3, -3, -2, -1, 0 };
            int[] result = new int[] { -3, -2, -1, 0, 1, 2, 3 };
            Analysis.Sort(smallArray);
            for (int i = 0; i < smallArray.Length; i++)
            {
                Assert.AreEqual(result[i], smallArray[i]);
            }
        }

        [TestMethod]
        public void randomizearraytest()
        {
            const int array_size = 1024;
            const int seed = 2342351;
            const int magnitude = 256;
            int[] array = new int[array_size];

            // makerandom(int[] array, int magnitude, int seed, bool includenegativevalues)
            //  int[] array to make random.
            //  int magnitude is one larger than the largest absolute value of 
            //     any item in the sequence.
            //  int seed is the seed to use to generate the sequence. 
            //  bool includenegativevalues is true if negative values should
            //    be included in the sequence.
            //  examples:
            //      // to produce random elements in the range 0<=x<256
            //      makerandom(myarray, 256, 234311, false);
            //
            //      // to produce random elements in the range -512<x<512
            //      makerandom(myarray, 512, 234311, true);
            //
            Analysis.MakeRandom(array, magnitude, seed, false);
            for (int i = 0; i < array.Length; i++)
            {
                Assert.IsTrue(array[i] >= 0 && array[i] < magnitude);
            }
        }



        [TestMethod]
        public void randomizeSameSeedTest()
        {
            const int ARRAY_SIZE = 1024;
            const int SEED = 2342351;
            const int MAGNITUDE = 512;
            int[] array = new int[ARRAY_SIZE];
            int[] anotherArray = new int[ARRAY_SIZE];

            // MakeRandom(int[] array, int magnitude, int seed, bool includeNegativeValues)
            //  int[] array to make random.
            //  int magnitude is one larger than the largest absolute value of 
            //     any item in the sequence.
            //  int seed is the seed to use to generate the sequence. 
            //  bool includeNegativeValues is true if negative values should
            //    be included in the sequence.
            //  Examples:
            //      // To produce random elements in the range 0<=x<256
            //      MakeRandom(myArray, 256, 234311, false);
            //
            //      // To produce random elements in the range -512<x<512
            //      MakeRandom(myArray, 512, 234311, true);
            //
            Analysis.MakeRandom(array, MAGNITUDE, SEED, true);
            Analysis.MakeRandom(anotherArray, MAGNITUDE, SEED, true);
            for (int i = 0; i < array.Length; i++)
            {
                Assert.AreEqual(array[i], anotherArray[i]);
            }
        }

        [TestMethod]
        // Search for an item in the array, return the index
        // If the index is not found return -1
        public void testLinearSearch()
        {
            int[] myArray = new int[] { 3, 5, 6, 8, 9, 10, 13, 151, 2521 };
            Assert.AreEqual(0, Analysis.LinearSearch(myArray, 3));
            Assert.AreEqual(1, Analysis.LinearSearch(myArray, 5));
            Assert.AreEqual(2, Analysis.LinearSearch(myArray, 6));
            Assert.AreEqual(3, Analysis.LinearSearch(myArray, 8));
            Assert.AreEqual(4, Analysis.LinearSearch(myArray, 9));
            Assert.AreEqual(5, Analysis.LinearSearch(myArray, 10));
            Assert.AreEqual(6, Analysis.LinearSearch(myArray, 13));
            Assert.AreEqual(7, Analysis.LinearSearch(myArray, 151));
            Assert.AreEqual(8, Analysis.LinearSearch(myArray, 2521));
            Assert.AreEqual(-1, Analysis.LinearSearch(myArray, 7));
            Assert.AreEqual(-1, Analysis.LinearSearch(myArray, 2522));
            Assert.AreEqual(-1, Analysis.LinearSearch(myArray, 0));
        }

        [TestMethod]
        // Search for an item in the array, return the index
        // If the index is not found return -1
        public void testBinarySearch()
        {
            int[] myArray = new int[] { 3, 5, 6, 8, 9, 10, 13, 151, 2521 };
            Assert.AreEqual(0, Analysis.BinarySearch(myArray, 3));
            Assert.AreEqual(1, Analysis.BinarySearch(myArray, 5));
            Assert.AreEqual(2, Analysis.BinarySearch(myArray, 6));
            Assert.AreEqual(3, Analysis.BinarySearch(myArray, 8));
            Assert.AreEqual(4, Analysis.BinarySearch(myArray, 9));
            Assert.AreEqual(5, Analysis.BinarySearch(myArray, 10));
            Assert.AreEqual(6, Analysis.BinarySearch(myArray, 13));
            Assert.AreEqual(7, Analysis.BinarySearch(myArray, 151));
            Assert.AreEqual(8, Analysis.BinarySearch(myArray, 2521));
            Assert.AreEqual(-1, Analysis.BinarySearch(myArray, 7));
            Assert.AreEqual(-1, Analysis.BinarySearch(myArray, 2522));
            Assert.AreEqual(-1, Analysis.BinarySearch(myArray, 0));
        }
    }
}

